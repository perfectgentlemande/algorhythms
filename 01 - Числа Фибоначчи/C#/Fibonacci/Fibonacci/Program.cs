﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        static int Fibonacci1 (int n)
        {
            if (n==0 || n==1)
            {
                return 1;
            }
            return Fibonacci1(n - 1) + Fibonacci1(n - 2);
        }
        static void Main(string[] args)
        {
            Console.WriteLine(Fibonacci1(4));
        }
    }
}
