def fibonacci1(n):
    if n==0 or n==1:
        return 1
    return fibonacci1(n-1)+fibonacci1(n-2)

print(fibonacci1(4))
