﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        static int Fibonacci2 (int n)
        {
            if (n==0)
            {
                return 1;
            }
            int previousNumber = 1;
            int currentNumber = 1;
            for (int i=2; i<=n; i++)
            {
                int tempNumber = currentNumber;
                currentNumber += previousNumber;
                previousNumber = tempNumber;
            }
            return currentNumber;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(Fibonacci2(4));
        }
    }
}
