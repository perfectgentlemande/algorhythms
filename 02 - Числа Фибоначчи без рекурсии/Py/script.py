def fibonacci2(n):
    if n==0:
        return 1

    previousNumber = 1
    currentNumber = 1
    for counter in range(2, n+1):
        tempNumber=currentNumber
        currentNumber+=previousNumber
        previousNumber=tempNumber
    return currentNumber

print(fibonacci2(4))
