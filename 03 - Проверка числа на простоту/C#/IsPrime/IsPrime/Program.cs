﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IsPrime
{
    class Program
    {
        static bool IsPrime(int n)
        {
            if (n==1)
            {
                return false;
            }
            for (int i=2; i*i<=n; i++)
            {
                if(n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        static void Main(string[] args)
        {
            //False
            Console.WriteLine(IsPrime(123));
            //True
            Console.WriteLine(IsPrime(127));
            //True
            Console.WriteLine(IsPrime(131));
            //False
            Console.WriteLine(IsPrime(132));
        }
    }
}
