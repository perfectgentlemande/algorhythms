function isPrime(n) {
	if (n===1) {
		return false;
	}
	for (var i = 2; i*i<=n; i++) {
		if (n%i === 0) {
			return false;
		}
	}
	return true;
}
//False
alert(isPrime(123));
//True
alert(isPrime(127));
//True
alert(isPrime(131));
//False
alert(isPrime(132));