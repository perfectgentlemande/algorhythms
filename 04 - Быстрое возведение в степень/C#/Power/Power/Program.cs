﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Power
{
    class Program
    {
        static double Power(double a, int n)
        {

            double result = 1;
            double tempAinPowerOf2 = a;

            while(n>0)
            {
                if ((n&1)==1)
                {
                    result *= tempAinPowerOf2;
                }
                tempAinPowerOf2 *= tempAinPowerOf2;
                n = n >> 1;
            }
            return result;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(Power(2,4));
        }
    }
}
