function power(a, n) {
	
	var result = 1;
	var tempAinPowerOf2 = a;
	
	while (n>0) {
		if((n&1)==1) {
			result*=tempAinPowerOf2;
		}
		tempAinPowerOf2*=tempAinPowerOf2;
		n=n>>1;
	}
	return result;
}

alert(power(2,4));
