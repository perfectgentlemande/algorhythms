def power(a, n):

    result = 1
    tempAinPowerOf2 = a

    while n>0:
       if (n&1)==1:
           result*=tempAinPowerOf2
       tempAinPowerOf2*=tempAinPowerOf2
       n=n>>1
    return result

print(power(2,4))
