﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HasElement
{
    class Program
    {
        static bool HasElement(double[] arr, double element)
        {
            for (int i=0; i<arr.Length; i++)
            {
                if (arr[i]==element)
                {
                    return true;
                }
            }
            return false;
        }
        static void Main(string[] args)
        {
            double[] arr = { 1, 2, 3, 4, 5 };

            //True
            Console.WriteLine(HasElement(arr, 3));

            //False
            Console.WriteLine(HasElement(arr, 6));
        }
    }
}
