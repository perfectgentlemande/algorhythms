function hasElement(arr, element) {
	for (var i=0; i<arr.length; i++) {
		if(arr[i]==element) {
			return true;
		}
	}
	return false;
}

var arr = [1, 2, 3, 4, 5];

//True
alert(hasElement(arr, 3));

//False
alert(hasElement(arr, 6));
