def hasElement(lst, selectedElement):
    for item in lst:
        if item==selectedElement:
            return True
    return False

lst = [1,2,3,4,5]

# true
print(hasElement(lst, 3))

# false
print(hasElement(lst, 6))
