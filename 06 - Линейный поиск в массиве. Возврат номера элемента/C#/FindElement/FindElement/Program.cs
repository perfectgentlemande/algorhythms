﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindElement
{
    class Program
    {
        static int FindElement(double[] arr, double element)
        {
            for (int i=0; i<arr.Length; i++)
            {
                if (arr[i]==element)
                {
                    return i;
                }
            }
            return -1;
        }
        static void Main(string[] args)
        {
            double[] arr = { 1, 2, 3, 4, 5 };

            //2
            Console.WriteLine(FindElement(arr, 3));

            //-1
            Console.WriteLine(FindElement(arr, 6));
        }
    }
}
