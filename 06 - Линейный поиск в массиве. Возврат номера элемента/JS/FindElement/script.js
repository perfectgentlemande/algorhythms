function findElement(arr, element) {
	for (var i=0; i<arr.length; i++) {
		if(arr[i]==element) {
			return i;
		}
	}
	return -1;
}

var arr = [1, 2, 3, 4, 5];

//2
alert(findElement(arr, 3));

//-1
alert(findElement(arr, 6));
