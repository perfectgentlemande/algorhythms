def findElement(lst, selectedElement):

    counter=0
    
    for item in lst:
        if item==selectedElement:
            return counter
        counter+=1
    return -1

lst = [1,2,3,4,5]

# 2
print(findElement(lst, 3))

# -1
print(findElement(lst, 6))
