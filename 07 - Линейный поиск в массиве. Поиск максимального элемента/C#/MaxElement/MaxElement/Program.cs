﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxElement
{
    class Program
    {
        static double MaxElement(double[] arr)
        {
            double currentMax = arr[0];

            for (int i=1; i<arr.Length; i++)
            {
                if (arr[i] > currentMax)
                {
                    currentMax = arr[i];
                }
            }

            return currentMax;
        }

        static void Main(string[] args)
        {
            double[] arr = { 1, 2, 3, 4, 4 };

            Console.WriteLine(MaxElement(arr));
        }
    }
}
