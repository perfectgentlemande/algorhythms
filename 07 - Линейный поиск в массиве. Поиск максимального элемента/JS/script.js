function maxElement(arr) {
	var currentMax = arr[0];
	
	for (var i=1; i<arr.length; i++) {
		if(currentMax<arr[i]) {
			currentMax=arr[i];
		}
	}
	return currentMax;
}

var arr = [1, 2, 3, 4, 4];

//4
alert(maxElement(arr));