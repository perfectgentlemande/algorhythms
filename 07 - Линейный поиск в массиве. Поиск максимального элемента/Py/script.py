def maxElement(lst):

    currentMaxItem=lst[0]

    for item in lst:
        if item>currentMaxItem:
            currentMaxItem=item
    return currentMaxItem

lst = [1,2,3,4,4]

# 4
print(maxElement(lst))
