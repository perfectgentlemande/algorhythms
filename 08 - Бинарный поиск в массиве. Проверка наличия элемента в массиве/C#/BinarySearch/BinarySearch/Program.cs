﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearch
{
    class Program
    {
        //Бинарный поиск без рекурсии
        static int BinarySearch(double[] arr, double elem)
        {
            int first = 0;
            int last = arr.Length;

            if (elem<arr[0] || elem > arr[arr.Length-1])
            {
                return -1;
            }

            while (first<last)
            {
                int mid = first + (last - first) / 2;

                if (elem<=arr[mid])
                {
                    last = mid;
                }
                else
                {
                    first = mid + 1;
                }
            }

            return first;
        }
        static void Main(string[] args)
        {
            double[] arr = { 1, 2, 3, 4, 4 };

            Console.WriteLine(BinarySearch(arr, 4));
        }
    }
}
