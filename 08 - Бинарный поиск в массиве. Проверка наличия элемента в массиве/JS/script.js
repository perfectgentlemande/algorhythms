function binarySearch(arr, elem) {
	
	var first = 0;
	var last = arr.length;
	
	if (elem < arr[0] || elem > arr[arr.length-1]) {
		return -1;
	}
	
	while(first < last) {
		
		var mid = first + Math.floor((last - first)/2);
		
		if (elem <= arr[mid]) {
			last = mid;
		}
		else {
			first = mid + 1;
		}
	}

	return first;	
}


var arr = [1, 2, 3, 4, 4];

//3
alert(binarySearch(arr, 4));