def binarySearch(lst, elem):

    first = 0
    last = len(lst)

    if (elem<lst[first] or elem>lst[last-1]):
        return -1

    while (first<last):
        mid = first + (last - first)//2
        if elem <= lst[mid]:
            last = mid
        else:
            first = mid+1
    return first

lst = [1,2,3,4,4]

# 3
print(binarySearch(lst, 4))
