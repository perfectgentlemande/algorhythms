﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearch2
{
    class Program
    {
        //Возвращает позицию вставки элемента на отрезке [first, last)
        static int FindInsertionPoint(double[] arr, int first, int last, double elem)
        {
            if(last - first == 1)
            {
                if (elem <= arr[first])
                {
                    return first;
                }
                return last;
            }

            int mid = (first + last) / 2;

            if (elem <= arr[mid])
            {
                return FindInsertionPoint(arr, first, mid, elem);
            }
            else
            {
                return FindInsertionPoint(arr, mid, last, elem);
            }
        }

        //Возвращает позицию элемента в упорядоченном массиве, если он есть.
        static int BinarySearch2(double[] arr, double elem)
        {
            if (arr.Length == 0)
            {
                return -1;
            }

            int point = FindInsertionPoint(arr, 0, arr.Length, elem);

            if (point == arr.Length || arr[point]!=elem)
            {
                return -1;
            }

            return point;
        }
        static void Main(string[] args)
        {
            double[] arr = { 1, 2, 3, 4, 4 };

            Console.WriteLine(BinarySearch2(arr, 4));

        }
    }
}
