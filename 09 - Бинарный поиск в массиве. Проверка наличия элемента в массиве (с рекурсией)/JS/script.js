//Возвращает позицию вставки элемента на отрезке [first, last)
function findInsertionPoint(arr, first, last, elem) {
	
	if(last - first == 1) {
		if (elem <= arr[first]) {
			return first;
		}
		return last;
	}
	
	var mid = Math.floor((first + last)/2);
	
	if (elem <= arr[mid]) {
		return findInsertionPoint(arr, first, mid, elem);
	}
	else {
		return findInsertionPoint(arr, mid , last, elem);
	}
}

//Возвращает позицию элемента в упорядоченном массиве, если он есть.
function binarySearch2(arr, elem) {
	
	if (arr.length == 0) {
		return -1;
	}
	
	var point = findInsertionPoint(arr, 0, arr.length, elem);
	
	if (point == arr.length || arr[point] != elem) {
		return -1;
	}
	
	return point;	
}


var arr = [1, 2, 3, 4, 4];

//3
alert(binarySearch2(arr, 4));