def findInsertionPoint(lst, first, last, elem):

    if (last - first == 1):
        if (elem <= lst[first]):
            return first
        return last

    mid = (first+last)//2

    if (elem<=lst[mid]):
        return findInsertionPoint(lst, first, mid, elem)
    else:
        return findInsertionPoint(lst, mid, last, elem)

def binarySearch2(lst, elem):

    if (len(lst) == 0):
        return -1

    point = findInsertionPoint(lst, 0, len(lst), elem)

    if (point == len(lst) or lst[point]!=elem):
        return -1

    return point

lst = [1,2,3,4,4]

# 3
print(binarySearch2(lst, 4))
