﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CArray
{

    //СД "Динамический массив"
    class CArray
    {
        private double[] buffer=new double[0];
        private int bufferSize;
        private int actualSize;

        public int LengthActual
        {
            get
            {
                return actualSize;
            }
        }
        public int Length
        {
            get
            {
                return buffer.Length;
            }
        }

        public CArray(double[] array, int userDefinedBufferSize)
        {
            bufferSize = userDefinedBufferSize;
            actualSize = array.Length;
            buffer = new double[(int)Math.Ceiling((decimal)array.Length / bufferSize)*3];

            for(int i=0; i<array.Length; i++)
            {
                buffer[i] = array[i];
            }
        }

        public void Add(double element)
        {
            if (actualSize+1>buffer.Length)
            {
                actualSize++;

                double[] newBuffer = new double[buffer.Length + 1];
                for (int i=0; i<buffer.Length; i++)
                {
                    newBuffer[i] = buffer[i];
                }

                newBuffer[buffer.Length] = element;
                
                buffer = new double[buffer.Length + bufferSize];

                for (int i=0; i<newBuffer.Length; i++)
                {
                    buffer[i] = newBuffer[i];
                }

            }

        }

        public double GetAt(int index)
        {
            return buffer[index];
        }


        public double this[int index]
        {
            get
            {
                return GetAt(index);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            double[] numbers = { 0.1, 3.2, 5.4, 3.5, 2.21, 0.5};

            CArray arr = new CArray(numbers, 3);

            //Исходный массив
            for (int i=0; i<arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Console.WriteLine("Actual size: " + arr.LengthActual+"\n");
            Console.WriteLine("Buffer size: " + arr.Length + "\n");

            arr.Add(99.99);

            //Измененный массив
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Console.WriteLine("Actual size: " + arr.LengthActual + "\n");
            Console.WriteLine("Buffer size: " + arr.Length + "\n");
        }
    }
}
