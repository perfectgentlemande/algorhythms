﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNode
{
    // Двусвязный список
    class DoublyNode<T>
    {
        public T Data { get; set; }
        public DoublyNode<T> PreviousNode { get; set; }
        public DoublyNode<T> NextNode { get; set; }

        public DoublyNode(T data)
        {
            Data = data;
        }
    }

    class DoublyLinkedList<T> : IEnumerable<T>
    {
        private DoublyNode<T> headNode;
        private DoublyNode<T> tailNode;
        private int count;

        public int Count { get { return count; } }
        public bool isEmpty { get { return count == 0; } }

        public void Add(T data)
        {
            DoublyNode<T> newNode = new DoublyNode<T>(data);

            if (headNode == null)
            {
                headNode = newNode;
            }
            else
            {
                tailNode.NextNode = newNode;
                tailNode.NextNode.PreviousNode = tailNode;
            }

            tailNode = newNode;
            count++;
        }

        public void AddFirst(T data)
        {
            DoublyNode<T> newNode = new DoublyNode<T>(data);
            DoublyNode<T> tempNode = headNode;

            newNode.NextNode = tempNode;
            headNode = newNode;

            if (count == 0)
            {
                tailNode = headNode;
            }
            else
            {
                tempNode.PreviousNode = newNode;
            }
            count++;
        }

        public bool Remove (T data)
        {
            DoublyNode<T> currentNode = headNode;

            while (currentNode != null)
            {
                if (currentNode.Data.Equals(data))
                {
                    break;
                }
                currentNode = currentNode.NextNode;
            }
            if (currentNode != null)
            {
                if (currentNode.NextNode != null)
                {
                    currentNode.NextNode.PreviousNode = currentNode.PreviousNode;
                }
                else
                {
                    tailNode = currentNode.PreviousNode;
                }
                if (currentNode.PreviousNode != null)
                {
                    currentNode.PreviousNode.NextNode = currentNode.NextNode;
                }
                else
                {
                    headNode = currentNode.NextNode;
                }
                count--;
                return true;
            }
            return false;
        }

        public void ClearList()
        {
            headNode = null;
            tailNode = null;
            count = 0;
        }

        public bool Contains(T data)
        {
            DoublyNode<T> currentNode = headNode;

            while (currentNode!=null)
            {
                if (currentNode.Data.Equals(data))
                {
                    return true;
                }
                currentNode = currentNode.NextNode;
            }
            return false;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            DoublyNode<T> currentNode = headNode;
            while (currentNode != null)
            {
                yield return currentNode.Data;
                currentNode = currentNode.NextNode;
            }
        }

        public IEnumerable<T> BackEnumerator()
        {
            DoublyNode<T> currentNode = tailNode;
            while (currentNode != null)
            {
                yield return currentNode.Data;
                currentNode = currentNode.PreviousNode;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DoublyLinkedList<string> linkedList = new DoublyLinkedList<string>();
            // добавление элементов
            linkedList.Add("Bob");
            linkedList.Add("Bill");
            linkedList.Add("Tom");
            linkedList.AddFirst("Kate");
            foreach (var item in linkedList)
            {
                Console.WriteLine(item);
            }
            // удаление
            linkedList.Remove("Bill");

            // перебор с последнего элемента
            foreach (var t in linkedList.BackEnumerator())
            {
                Console.WriteLine(t);
            }
        }
    }
}
