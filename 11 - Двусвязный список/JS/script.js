function DoublyNode(data) {
	
	this.data = data;
	this.previousNode = undefined;
	this.nextNode = undefined;
}

function DoublyLinkedList() {
	
	var headNode = new DoublyNode(undefined);
	var tailNode = new DoublyNode(undefined);

	var count = 0;
	
	this.add = function(data) {
		
		var newNode = new DoublyNode(data);
		
		if (headNode.data === undefined) {
			headNode = newNode;
		}
		else {
			tailNode.nextNode = newNode;
			tailNode.nextNode.previousNode = tailNode;
		}
		
		tailNode = newNode;
		count++;
	}
	
	this.addFirst = function(data) {
		
		var newNode = new DoublyNode(data);
		var tempNode = headNode;
		
		newNode.nextNode = tempNode;
		headNode = newNode;
		
		if(count == 0) {
			tailNode = headNode;
		}
		else {
			tempNode.previousNode = newNode;
		}
		count++;
	}
	
	this.remove = function(data) {
		
		var currentNode = headNode;
		
		while (currentNode !== undefined) {
			
			if (currentNode.data == data) {
				break;
			}
			currentNode = currentNode.nextNode;
		}
		
		if (currentNode !== undefined) {
			
			if (currentNode.nextNode !== undefined) {
				
				currentNode.nextNode.previousNode = currentNode.previousNode;
			}
			else {
				
				tailNode = currentNode.previousNode;
			}
			if (currentNode.previousNode !== undefined) {
				
				currentNode.previousNode.nextNode = currentNode.nextNode;
			}
			else {
				
				headNode = currentNode.nextNode;
			}
			count--;
			return true;
		}
		return false;
	}
	
	this.clearList = function() {
		
		headNode = undefined;
		tailNode = undefined;
		count = 0;
	}
	
	this.contains = function(data) {
		
		var currentNode = headNode;
		
		while(currentNode !== undefined) {
			
			if (currentNode.data == data) {
				
				return true;
			}
			currentNode = currentNode.nextNode;
		}
		
		return false;
	}
	
	this.showList = function() {
		
		var currentNode = headNode;
		
		while(currentNode !== undefined) {
			alert(currentNode.data);
			currentNode = currentNode.nextNode;
		}
	}
}

var doublyLinkedList = new DoublyLinkedList();
doublyLinkedList.add("Bob");
doublyLinkedList.add("Bill");
doublyLinkedList.add("Tom");
doublyLinkedList.add("Kate");


alert(doublyLinkedList.contains("Bob"));
doublyLinkedList.showList();
doublyLinkedList.addFirst("Beb");
doublyLinkedList.showList();
doublyLinkedList.remove("Beb");
doublyLinkedList.showList();